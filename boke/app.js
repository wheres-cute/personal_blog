//app.js
App({
  // 用户头像、昵称、所在地、性别
  userInfo:{},
  // 用户的openID、session_key
  userInfos:{},
  // 存放用户的登录状态(账号绑定状态)
  bindInfo:{},
  webUrl:"http://www.uekblog.com/",
  // https://yiweifen.com/html/news/zhiyezige/63572.html
  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {
    // 1、在小程序中，我们使用openid来标识用户
    // 2、每一个用户相对于小程序都有不同的openid
    // 3、使用openid干什么？
    //    使用openid关联到我们blog原有的用户体系

    // 获取openid

    // 1、获取临时的登陆凭证
    let that = this;
    wx.login({
      success:function(res){
        // 获取用户登陆的临时凭证
        let code = res.code;
        // 发送ajax请求获取openid
        
        // 2、请求我们自己的地址，获取openid
        wx.request({
          // 请求地址
          url: `${that.webUrl}/index.php/api/index/getopenid?code=${code}`,
          // 请求的设置
          header:{
            "content-type":"application/json"
          },
          // 请求成功
          success:function(res){
            that.userInfos.openid = res.data.openid;
            that.userInfos.session_key = res.data.session_key;

            // 3、检测用户是否绑定
            wx.request({
              // 请求地址
              url:  `${that.webUrl}/index.php/api/index/checkbind?openid=${res.data.openid}`,
              // 请求的设置
              header: {
                "content-type": "application/json"
              },
              // 请求成功
              success:function(res){
                that.bindInfo=res.data;

                console.log(that);
              }
              
            })
          }
        });
      }
    });

    // 获取用户的授权信息
    wx.getSetting({
      success:function(res){
        
        // 判断用户是否已经授权
        if(res.authSetting['scope.userInfo']){
          // 获取用户信息
          wx.getUserInfo({
            lang:"zh_CN",
            success:function(res){
              that.userInfo = res.userInfo
            }
          })  
        }
      }
    });

  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {
    
  },

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {
    
  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function (msg) {
    
  },

  
})
