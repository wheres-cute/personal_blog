// pages/article/article.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    webUrl: app.webUrl,
    id: "",
    article: '',
    comment: [],
    bindInfo: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取文章ID
    let id = options.id;
    // 设置给data
    this.setData({
      id: id
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {


    // 发送ajax请求 获取页面数据
    let that = this;

    that.setData({
      bindInfo: app.bindInfo
    });
    wx.request({
      // 请求地址
      url: `${app.webUrl}/index.php/api/article/index?id=${that.data.id}`,
      // 请求类型
      header: {
        'content-type': 'application/json' // 默认值
      },

      // 响应结果
      success: function (res) {

        // 动态的设置网站标题

        wx.setNavigationBarTitle({
          title: res.data.title
        });

        // 在js中进行字符串替换
        let str = res.data.text.replace(/\/ueditor\//g, app.webUrl + '/ueditor/');

        // js进行正则匹配替换替换

        str = str.replace(/<img src/g, "<img style='width:100%' src");

        // 将数据设置
        res.data.text = str;

        // 设置数据

        that.setData({
          article: res.data
        });

        // 设置历史记录
        // 1、获取历史记录
        let seeHistory = wx.getStorageSync("seeHistory") ? wx.getStorageSync("seeHistory") : [];

        // 2、将浏览数据格式化
        let time = new Date();
        let info = {
          id: res.data.id,
          title: res.data.title,
          img: res.data.img,
          time: `${time.getFullYear()}-${time.getMonth()+1}-${time.getDate()} ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`
        };

        // 3、将数据追加到数组中
        let newArr = [];
        // 遍历实例记录
        seeHistory.forEach(function (value) {
          // 判断历史记录中是否有当前文章
          if (value.id == res.data.id) {

          } else {
            newArr.push(value);
          }
        });
        newArr.unshift(info);

        // 4、将数据设置到缓存中

        wx.setStorageSync("seeHistory", newArr);

      }
    });

    // 发送ajax请求获取文章对应品论
    that.getComment();

    // 发送ajax请求获取当前用户的是否点赞和收藏

    that.getStatus();
  },

  getStatus: function () {
    // 获取文章和用户id

    let nid = this.data.id;
    let uid = app.bindInfo.userId;
    let that = this;
    // 发送ajax请求获取状态
    // 如果用户未绑定
    if (uid == 0) {
      this.setData({
        isCollect: false,
        isZan: false
      });
    } else {
      // 如果用户绑定
      wx.request({
        url: `${app.webUrl}/index.php/api/user/getstatus`,
        header: {
          "content-type": "application/json"
        },
        data: {
          nid: nid,
          uid: uid
        },
        success: function (res) {

          // 设置数据
          that.setData({
            isCollect: res.data.isCollect,
            isZan: res.data.isZan
          });
        }
      })
    }


  },
  // 取消点赞的事件
  quxiaozan: function () {
    // 获取用户的ID
    let nid = this.data.id;
    // 获取文章的ID
    let uid = app.bindInfo.userId;

    // 发送ajax请求取消点赞
    let that = this;
    wx.request({
      url: `${app.webUrl}/index.php/api/user/quxiaozan`,
      header: {
        "content-type": "application/json"
      },
      data: {
        nid: nid,
        uid: uid
      },
      success: function (res) {
        // 判断是否成功
        if (res.data.code == 200) {
          // 取消成功
          wx.showToast({
            title: res.data.info,
          });
          that.getStatus();
        } else {
          // 取消失败
          wx.showToast({
            title: res.data.info,
            icon: "none"
          })
        }
      }
    })
  },

  // 用户点赞的事件

  dianzan: function () {
    // 获取用户的ID
    let nid = this.data.id;
    // 获取文章的ID
    let uid = app.bindInfo.userId;

    // 如果用户未绑定，请绑定
    if (uid == 0) {
      wx.navigateTo({
        url: '/pages/my/bind',
      });

      return "";
    }

    let that = this;
    // 发送ajax请求完成点赞功能

    wx.request({
      url: `${app.webUrl}/index.php/api/user/dianzan`,
      header: {
        'content-type': "application/json"
      },
      data: {
        nid: nid,
        uid: uid
      },
      success: function (res) {
        // 根据处理结果进行不同操作
        if (res.data.code == 200) {
          // 点赞成功

          wx.showToast({
            title: res.data.info,
          });

          that.getStatus();

        } else {
          wx.showToast({
            title: res.data.info,
            icon: "none"
          });
        }
      }
    })
  },
  // 获取文章评论对的方法
  getComment: function () {
    let that = this;
    wx.request({
      url: `${app.webUrl}/index.php/api/article/comment?id=${that.data.id}&uid=${app.bindInfo.userId}`,
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // 设置数据

        that.setData({
          comment: res.data
        })
      }
    })
  },
  // 评论提交的方法
  commentSubmit: function (e) {
    // 判断我是否可以发表评论
    let nowTime = new Date().getTime();
    if (app.commentTime && nowTime < (app.commentTime + 4 * 60 * 1000)) {
      wx.showToast({
        title: '点击频繁',
        icon: "none"
      });

      return '';
    }

    // 获取评论内容
    let text = e.detail.value.text;
    // 文章ID
    let nid = this.data.id;
    // 用户ID
    let uid = app.bindInfo.userId;

    // 判断内容是否为空

    if (!text) {
      wx.showToast({
        title: '发表内容不能为空',
        icon: "none"
      });

      return "";
    }
    // 发送ajax请求
    let that = this;
    wx.request({
      // 请求地址
      url: `${app.webUrl}/index.php/api/user/facomment`,
      // 请求的header头
      header: {
        "content-type": "application/json"
      },
      //请求数据
      data: {
        text: text,
        nid: nid,
        uid: uid
      },
      // 请求方式
      method: "post",
      success: function (res) {
        // 判断是否发表成功
        // 发表成功
        if (res.data.code == 200) {
          let time = new Date();
          app.commentTime = time.getTime();
          wx.showToast({
            title: res.data.info,
            success: function () {
              that.getComment();
            }
          })
        } else {
          // 发表失败
          wx.showToast({
            title: res.data.info,
          })
        }
      }

    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})