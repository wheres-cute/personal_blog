// pages/news/news.js

// 获取app对象

let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  p: 2,
  data: {
    webUrl: app.webUrl,
    swiperImage: [],

    tuiJian: [

    ],

    faBu: [

    ],
    indicatorDots: true,
    autoplay: true,
    interval: 2000,
    duration: 500,
    show: '',
    isBottom: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 页面重新加载设置页码
    this.p = 2;
    // 页面重新加载设置是否显示底部
    this.setData({
      isBottom: false
    });
    // 调用获取数据的方法
    this.getData();
  },

  // 搜索方法

  search: function (e) {

    // 获取搜搜的内容
    let search = e.detail.value;

    // 判断搜索的内容是否存在

    if (search) {
      // 跳转到搜索展示页面
      wx.navigateTo({
        url: '/pages/search/search?name=' + search,
      })

    } else {
      // 提示信息
      wx.showToast({
        title: '搜索内容不能为空',
        icon: "none"
      });
    }

  },

  getData: function () {
    let that = this;
    // 发送ajax请求获取首页轮播图数据

    wx.request({
      url: app.webUrl + '/index.php/Api/Index/slider',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // 设置数据
        that.setData({
          swiperImage: res.data
        });
      }
    });

    // 获取推荐新闻

    wx.request({
      // 请求的地址
      url: app.webUrl + '/index.php/Api/Index/hot',
      // 返回的数据格式
      header: {
        'content-type': 'application/json' // 默认值
      },
      // 成功的处理

      success: function (res) {
        // 修改data中的数据

        that.setData({
          tuiJian: res.data
        });
      }
    });

    // 发送ajax请求获取最新发布新闻

    wx.request({
      url: app.webUrl + 'index.php/Api/Index/newstime',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // 修改data中的数据

        that.setData({
          faBu: res.data
        });
      }

    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // 调用下拉刷新方法
    this.getData();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function (res) {
    // 发送ajax请求获取下一页数据

    let that = this;

    // 如果已经到达底部，不在发送ajax请求
    if (that.data.isBottom) {
      return "";
    }

    // 发送ajax请求获取下一页数据
    wx.request({
      // 请求地址
      url: app.webUrl + 'index.php/Api/Index/newstime?page=' + this.p,
      //  请求header
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {

        // 判断data是否为空

        if (res.data.length == 0) {
          that.setData({
            isBottom: true
          })
        } else if (res.data.length < 5) {
          // res.data 最新新闻的下一页数据
          // 将获取到的下一页数据追加

          that.setData({
            faBu: that.data.faBu.concat(res.data),
            isBottom: true
          });

        } else {
          that.setData({
            faBu: that.data.faBu.concat(res.data)
          });

          // 设置页码
          that.p = ++that.p;
        }

      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: "优逸客个人博客",
      path: "/pages/news/news"
    };
  },
  // 回到顶部按钮显示
  onPageScroll: function (res) {
    let scroolTop = res.scrollTop;
    if (scroolTop > 200) {
      this.setData({
        "show": true
      })
    } else {
      this.setData({
        "show": false
      })
    }
  },
  // 回到顶部的方法
  return: function () {
    // 回到顶部
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 1000
    });
  }
})