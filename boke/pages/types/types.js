// pages/types/types.js

let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    webUrl: app.webUrl,
    types: [

    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    // 发送ajax请求获取数据

    wx.request({
      // 请求地址
      url: app.webUrl + '/index.php/Api/type/index',
      // 请求设置header
      header: {
        'content-type': 'application/json' // 默认值
      },
      // 响应成功
      success: function (res) {
        // res.data 就是分类数据
        that.setData({
          types: res.data
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})