/*
 * @Author: your name
 * @Date: 2020-08-19 11:46:53
 * @LastEditTime: 2020-08-19 15:17:05
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \personal_blog\boke\pages\history\history.js
 */
// pages/history/history.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    webUrl: app.webUrl,
    history: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取历史记录
    let seeHistory = wx.getStorageSync("seeHistory") ? wx.getStorageSync("seeHistory") : [];

    // 设置初始化的数据

    this.setData({
      history: seeHistory
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})