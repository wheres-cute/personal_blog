// pages/type/type.js
let app = getApp();
Page({
  // 1、接收地址栏上传递的id
  // 2、发送ajax请求获取所有的分类信息
  // 3、将ajax请求的分类数据展示到页面中
  // 4、判断菜单被选中
  // 5、完成获取分类下新闻接口的开发
  // 6、发送ajax请求获取分类新闻数据
  // 7、将数据展示到界面中，利用了两层wx:for 循环
  // 7.1 swiper 高度不能自动增加
  // 7.2 没有新闻数据展示，没有数据

  // 8、点击进入列表页面展示对应的新闻
  // 9、给菜单绑定点击事件,点击菜单完成选中菜单的切换和swiper的切换
  // 10、完成swiper切换

  /**
   * 页面的初始数据
   */
  data: {
    webUrl: app.webUrl,
    menu: [

    ],
    currentMenu: 0,
    show: '',
    id: 0,
    info: [],
    height: 1100


  },
  // 点击菜单事件

  clickMenu: function (res) {
    // 接受按钮的数据

    let index = res.target.dataset.index;
    let id = res.target.dataset.id;

    // 需要设置给data

    this.setData({
      id: id,
      currentMenu: index
    });
  },
  // swiper被改变的方法
  mainChange: function (res) {
    // 获取变化swiper的编号
    let index = res.detail.current;
    let id = this.data.menu[index].id;

    // 并且设置数据
    this.setData({
      id: id,
      currentMenu: index
    });

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    // 接受数据的ID
    let id = options.id;
    // 接受到数据的编号
    let num = options.index;

    // 设置我当前是那个id
    this.setData({
      id: id,
      currentMenu: num
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    let that = this;
    // 发送ajax请求获取所有的分类信息

    wx.request({
      // 请求地址
      url: `${app.webUrl}/index.php/api/type/index`,
      // header
      header: {
        'content-type': 'application/json' // 默认值
      },
      // 获取ajax返回的数据
      success: function (res) {

        // res.data 就是我们请求的数据
        that.setData({
          menu: res.data
        });
      }
    });

    // 获取新闻分类数据

    wx.request({
      // 请求地址
      url: `${app.webUrl}/index.php/api/type/getnews`,
      // header
      header: {
        'content-type': 'application/json' // 默认值
      },
      // 请求成功
      success: function (res) {
        // 设置数据给界面

        that.setData({
          info: res.data
        });
      }
    })


  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

})