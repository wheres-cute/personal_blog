/*
 * @Author: your name
 * @Date: 2020-08-19 11:46:53
 * @LastEditTime: 2020-08-19 15:17:14
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \personal_blog\boke\pages\logs\logs.js
 */
//logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    logs: []
  },
  onLoad: function () {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return util.formatTime(new Date(log))
      })
    })
  }
})