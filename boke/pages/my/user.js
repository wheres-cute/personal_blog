// pages/my/user.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 发送ajax请求获取用户信息
    // 显示加载中效果
    wx.showLoading({
      title: '加载中',
    });

    let that = this;
    wx.request({
      // 请求地址
      url: `${app.webUrl}/index.php/api/user/userinfo?id=${app.bindInfo.userId}`,
      // 设置请求头信息
      header: {
        "content-type": "application/json"
      },
      // 成功处理
      success: function (res) {
        // 隐藏加载中
        wx.hideLoading();
        that.setData({
          userinfo: res.data,
          userinfo2: app.userInfo,
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})