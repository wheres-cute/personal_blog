// pages/my/my.js
let app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: app.userInfo
  },
  // 用户授权获取基本信息
  getuserinfo: function (res) {

    // 获取用户的昵称和头像
    app.userInfo = res.detail.userInfo;

    this.setData({
      userInfo: app.userInfo
    });

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  // 内容改变
  mainChange: function (res) {
    let currentMenu = res.detail.current;
    this.setData({
      currentMenu: currentMenu
    })
  },
  // 点击菜单
  clickMenu: function (res) {
    // 获取锁点击菜单的编号
    let idx = res.target.dataset.index;
    //  设置当前显示的菜单
    this.setData({
      currentMenu: idx
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo: app.userInfo

    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  // 点击我的账户
  user: function () {
    this.isBind("/pages/my/user");
  },
  // 点击我的评论
  comment: function () {
    // 判断用户是否绑定
    this.isBind("/pages/my/comment");
  },

  // 点击我的收藏
  collect: function () {
    this.isBind("/pages/my/collect");
  },
  // 点击我的点赞
  prise: function () {
    this.isBind("/pages/my/prise");
  },

  // 判断用户是否绑定
  isBind: function (url) {
    if (!app.bindInfo.isBind) {
      wx.showToast({
        title: "请绑定账户",
        image: "/icon/error.png",
        duration: 1000,
        mask: true,
        success: function () {
          setTimeout(function () {
            wx.navigateTo({
              url: '/pages/my/bind',
            });
          }, 1000);

        }
      });
    } else {
      wx: wx.navigateTo({
        url: url,
      })
    }
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})