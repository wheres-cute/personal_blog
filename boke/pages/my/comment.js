/*
 * @Author: your name
 * @Date: 2020-08-19 11:46:53
 * @LastEditTime: 2020-08-19 15:17:29
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \personal_blog\boke\pages\my\comment.js
 */
// pages/my/comment.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    comment: [],
    webUrl: app.webUrl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    wx.showLoading({
      title: '加载中',
    });
    let that = this;
    // 发送ajax请求获取当前用户所有的评论信息
    wx.request({
      // 请求地址
      url: `${app.webUrl}/index.php/api/user/commentlist?id=${app.bindInfo.userId}`,
      // header头设置
      header: {
        "content-type": "application/json",
      },
      success: function (res) {

        // 设置数据
        that.setData({
          comment: res.data
        });

        wx.hideLoading();
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})