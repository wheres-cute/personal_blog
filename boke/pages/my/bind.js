// pages/my/bind.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fixeds: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  formSubmit: function (e) {
    // 获取用户的数据
    let phone = e.detail.value.phone;
    let password = e.detail.value.password;

    // 获取openid
    let openid = app.userInfos.openid;

    // 获取用户的昵称和头像

    let nickName = app.userInfo.nickName;
    let avatarUrl = app.userInfo.avatarUrl;

    // 判断如果用户没有昵称
    if (!nickName) {
      this.setData({
        fixeds: false
      });
    }

    // 发送ajax请求

    wx.request({
      // 请求地址
      url: `${app.webUrl}/index.php/api/user/index`,
      // 设置请求参数
      data: {
        phone: phone,
        password: password,
        openid: openid,
        nickName: nickName,
        avatarUrl: avatarUrl,
      },
      method: "POST",
      // header头
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        // 判断是否绑定成功

        if (res.data.code == 200) {
          // 设置账户绑定状态
          app.bindInfo = {
            isBind: true,
            userId: res.data.id
          };
          wx.switchTab({
            url: "/pages/my/my"
          });


        } else {
          wx.showToast({
            icon: "none",
            title: res.data.info
          });
        }

      }
    })



  },

  // 用户授权获取基本信息
  getuserinfo: function (res) {

    // 获取用户的昵称和头像
    app.userInfo = res.detail.userInfo;

    this.setData({
      fixeds: true
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})