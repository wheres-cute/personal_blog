/*
 * @Author: your name
 * @Date: 2020-08-19 11:46:53
 * @LastEditTime: 2020-08-19 15:17:37
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \personal_blog\boke\pages\my\prise.js
 */
// pages/my/prise.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    zanData: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取用户的ID

    let uid = app.bindInfo.userId;

    // 发送ajax请求获取点赞
    let that = this;
    wx.request({
      url: `${app.webUrl}/index.php/api/user/zanlist?id=${uid}`,
      header: {
        "content-type": "application/json"
      },
      success: function (res) {
        that.setData({
          zanData: res.data
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})