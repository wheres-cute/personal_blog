<?php 
// 声明命名空间
namespace app\api\controller;

// 导入系统控制器
use think\Controller;


use think\Db;

// 声明控制器

class User extends Controller{
	// 进行用户绑定

	public function index(){
		// 接收所有提交的数据

		$data = input("post.");

		// 接收数据

		$phone=$data['phone'];
		$password=md5($data['password']);
		$openid=$data['openid'];
		$nickName=$data['nickName'];
		$avatarUrl=$data['avatarUrl'];

		// 1、检测用户是否存在

		$phoneData = Db::table("user")->where("phone = '$phone'")->find();
		
		if ($phoneData) {

			// 2、检测用户账户和密码是否正确
			$userInfo = Db::table("user")->where("phone = '$phone' and password='$password' and status = 0")->find();
			
			if($userInfo){

				// 3、修改用户的数据

				$saveData=[
					"id"=>$userInfo["id"],
					"openid"=>$openid,
					"nickname"=>$nickName,
					"weixinimg"=>$avatarUrl,

				];

				$a = Db::table("user")->update($saveData);

				if ($a) {
					$arr=[
						"code"=>200,
						"info"=>"绑定账户成功",
						"id"=>$saveData['id'],
					];
				}else{
					$arr=[
						"code"=>400,
						"info"=>"绑定账户失败",
					];
				}

			}else{
				$arr=[
					"code"=>400,
					"info"=>"您输入的密码有误",
				];
			}
		}else{
			$arr=[
				"code"=>400,
				"info"=>"您输入的用户不存在",
			];
		}

		return json_encode($arr);
	}

	// 获取用户信息

	public function userinfo(){
		// 接收用户id
		$id = input("get.id");

		// 根据id获取用户信息

		$data = Db::table("user")->field("phone,nickname,time")->find($id);

		// 将时间进行格式化

		$data['time']=date("Y-m-d H:i:s",$data['time']);

		return json_encode($data);

	}

	// 获取用户对应的评论信息

	public function commentlist(){

		// 获取用户ID
		$id = input("get.id");

		// 通过用户ID 查找所有的评论

		$comment = Db::table("comment")
						->field("comment.*,news.title,news.img")
						->join("news","news.id = comment.nid")
						->where("comment.uid = $id")
						->limit(6)
						->select();

		// 将数据进行处理

		foreach ($comment as $key => &$value) {
			$value['img'] = str_replace('\\', '/', $value['img']);
			
			$value['times']=date("Y-m-d H:i:s",$value['time']);
		}

		return json_encode($comment);

	}

	// 发表评论
	public function facomment(){
		// 接受用户提交的数据
		// sql注入
		// 表单安全
		$data = input("post.");

		$data['text']=addslashes($data['text']);
		$data['time']=time();
		$data['status']=0;

		// 如何插入数据库

		if (Db::table("comment")->insert($data)) {
			$arr=[
				"code"=>200,
				"info"=>"发表成功",
			];
		}else{
			$arr=[
				"code"=>400,
				"info"=>"发表失败",
			];
		}

		return json_encode($arr);
	}

	// 完成点赞功能

	public function dianzan(){
		// 接受数据

		$userId = input("get.uid");
		$newsId = input("get.nid");

		// 查询对应的文章表和用户表

		$user = Db::table("user")->find($userId);
		$news = Db::table("news")->find($newsId);
		
		// 修改用户表的点赞字段

		$arr=[
			"id"=>$userId,
			"prise"=>$user['prise'].$newsId.","
		];

		$a=Db::table("user")->update($arr);

		// 修改文章表的点赞数量

		$brr=[
			"id"=>$newsId,
			"zannum"=>$news['zannum']+1
		];

		$b=Db::table("news")->update($brr);

		if ($a && $b) {
			$data = [
				"code"=>200,
				"info"=>"点赞成功",
			];
		}else{
			$data = [
				"code"=>400,
				"info"=>"点赞失败",
			];
		}

		return json_encode($data);

	}

	// 获取收藏和点赞状态

	public function getstatus(){

		// 接受数据

		$nid = input("get.nid");
		$uid = input("get.uid");

		// $nid=34;
		// $uid=25;

		// 获取用户的信息

		$user = Db::table("user")->find("$uid");

		$arr=[
			"isCollect"=>false,
			"isZan"=>false,
		];

		// 判断该文章是否点赞

		// 20,30,40,100,
		// ,10,
		if(strstr(",".$user['prise'], ",".$nid.",")){
			$arr['isZan']=true;
		}

		// 判断该文章是否收藏

		if(strstr(",".$user['collect'], ",".$nid.",")){
			$arr['isCollect']=true;
		}

		return json_encode($arr);



	}

	// 取消点赞方法

	public function quxiaozan(){
		// 接受数据

		$nid = input("get.nid");
		$uid = input("get.uid");

		$nid = 34;
		$uid=25;

		// 查询对应的用户信息和文章的信息

		$user = Db::table("user")->find($uid);
		$news = Db::table("news")->find($nid);

		// 字符串替换
		// 10,20,30,40,
		// 10
		// ,10,  ,10,20,30,40,
		// ,20,30,40,
		$str = str_replace(",$nid,", ",", ",".$user['prise']);
	
		$str = substr($str, 1);

		// 修改用户的点赞字段

		$arr = [
			"id"=>$uid,
			"prise"=>$str,
		];

		$a = Db::table("user")->update($arr);

		// 修改文章的点赞数量

		$brr=[
			"id"=>$nid,
			"zannum"=>$news['zannum']-1
		];

		$b=Db::table("news")->update($brr);

		if ($a && $b) {
			$info = [
				"code"=>200,
				"info"=>"取消成功",
			];
		}else{
			$info = [
				"code"=>400,
				"info"=>"取消失败",
			];
		}

		return json_encode($info);
	}

	// 获取用户的点赞列表

	public function zanlist(){
		// 接受用户发送的id

		$uid = input("get.id");

		$uid=25;

		// 查询用户点赞的所有内容

		$data = Db::table("user")->find($uid);
		$prise= $data['prise']."0";

		// 获取所有的点赞新闻

		$zanList = Db::table("news")->where("id in ($prise)")->select();

		return json_encode($zanList);


	}
}



 ?>