<?php 
// 声明命名空间
namespace app\api\controller;

// 导入系统控制器
use think\Db;
use think\Controller;

// 声明控制器

class Index extends Controller{
	// 首页方法

	public function index(){
		return "我是API模块Index控制器Index方法";
	}

	// 获取轮播图的接口

	public function slider(){
		// 获取轮播图信息
		$data = Db::table("slider")->order("sort desc")->select();

		// 将图片地址\替换成
		foreach ($data as $key => &$value) {
			$value['img']=str_replace('\\', '/', $value['img']);
		}

		// 返回数据
		return json_encode($data);

	}

	// 获取热门推荐文章

	public function hot(){

		// 从新闻表读取热门文章

		$data = Db::table("news")->order("num desc ")->limit(3)->select();
	
		// 转换斜线

		foreach ($data as $key => &$value) {
			
			$value['img'] = str_replace('\\', '/', $value['img']);
		}

		return json_encode($data);

	}

	// 获取最新发布的新闻

	public function newstime(){
		// 第一页 0,5
		// 第二页 5,10
		// 第三页 10,15
		// 接受地址栏页码

		$page = input("get.page",1);

		// 获取数据
		$data = Db::table("news")->order("time desc ")->page($page,5)->select();
		
		// 转换斜线

		foreach ($data as $key => &$value) {
			
			$value['img'] = str_replace('\\', '/', $value['img']);
			$value['times'] = date("Y-m-d H:i:s",$value['time']);
		}

		// 返回数据

		return json_encode($data);

	}

	// 获取openid的方法

	public function getopenid(){
		// 请求的url地址

		$appid="wxa2ce3e78b1eb93fe";
		$secret="37202c97f8e164ff054c4faee081cfbd";
		$js_code = input("get.code");

		$url="https://api.weixin.qq.com/sns/jscode2session?appid={$appid}&secret={$secret}&js_code={$js_code}&grant_type=authorization_code";

		// 使用file_get_contents方法获取 url 中的内容
		$data = file_get_contents($url);

		return $data;
	}

	// 判断用户是否被绑定

	public function checkbind(){
		// 接收openid
		$openid = input("get.openid");

		// 检测数据库中是否有被绑定

		$data = Db::table("user")->where("openid = '$openid' and status = 0")->find();

		// 判断$data 是否存在
		// 如果存在证明已经绑定用户

		if ($data) {
			$arr=[
				"isBind"=>true,
				"userId"=>$data['id']
			];
		}else{
			$arr=[
				"isBind"=>false,
				"userId"=>0
			];
		}

		return json_encode($arr);
	}

	// search 方法

	public function search(){
		// 接受用户搜索的关键字

		$search = input("get.name");

		// 搜索对应文章内容

		$data = Db::table("news")->where("title like '%$search%'")->order("num desc")->limit(6)->select();


		foreach ($data as $key => &$value) {
			
			$value['img'] = str_replace('\\', '/', $value['img']);
			$value['times'] = date("Y-m-d H:i:s",$value['time']);
		}
		// 转换json

		return json_encode($data);
	}
}



 ?>