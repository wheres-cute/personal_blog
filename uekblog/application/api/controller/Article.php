<?php 
// 声明命名空间
namespace app\api\controller;

// 导入系统控制器
use think\Controller;


use think\Db;

// 声明控制器

class Article extends Controller{
	// 获取新闻详情方法

	public function index(){
		// 接受参数
		$id = input("get.id");

		// 查询数据库

		$data = Db::table("news")->find($id);

		$data['times'] = date("Y-m-d H:i:s",$data['time']);
		$data['img'] = str_replace('\\', '/', $data['img']);

		// 增加文章的阅读量

		$arr=[
			"id"=>$id,
			"num"=>$data['num']+1
		];

		Db::table("news")->update($arr);
 
		return json_encode($data);

	}

	// 获取文章的评论

	public function comment(){
		// 获取地址栏文章的ID
		$id = input("get.id");
		$uid = input("get.uid");


		// 判断用户是否绑定
		// 没有绑定账户
		if ($uid==0) {
			# code...
			// 查询所有文章对应评论数据(审核过)

			$data = Db::table("comment")->field("comment.*,user.phone,user.nickname,user.img,user.weixinimg")->join("user","user.id = comment.uid")->where("comment.nid = $id and comment.status = 1")->limit(5)->order("id desc")->select();

		}else{
			// 我们已经绑定了账户
			// 查询所有审核过的评论和我自己的所有评论
			$data = Db::table("comment")
					->field("comment.*,user.phone,user.nickname,user.img,user.weixinimg")
					->join("user","user.id = comment.uid")
					->where("comment.nid = $id and comment.status = 1")
					->whereOr("comment.nid = $id and comment.uid = $uid")
					->limit(5)
					->order("id desc")
					->select();

		}

		// 将数据格式化

		foreach ($data as $key => &$value) {
			# code...
			$value['time']=date("Y-m-d H:i:s",$value['time']);
		}

		return json_encode($data);

	}

}



 ?>