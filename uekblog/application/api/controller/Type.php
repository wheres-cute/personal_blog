<?php 
// 声明命名空间
namespace app\api\controller;

// 导入系统控制器
use think\Controller;
use think\Db;

// 声明控制器

class Type extends Controller{
	// 方法

	public function index(){
		// 查询分类相关的数据

		$data = Db::table("newtype")->select();

		// 完成页面数据的分配

		return json_encode($data);
	}

	// 获取所有的分类对应新闻

	public function getnews(){
		// 查询分类相关的数据

		$data = Db::table("newtype")->select();


		// 查询分类对应的新闻


		foreach ($data as $key => $value) {

			$news= Db::table("news")->where("typeid = ".$value['id'])->order("id desc")->limit(5)->select();
			
			foreach ($news as $k => &$v) {
				# code...
				$v['img'] = str_replace('\\', '/', $v['img']);
				$v['times'] = date("Y-m-d H:i:s",$v['time']);
			}
			$data[$key]['news']=$news;

		}


		
		// 将数据转换成json

		return json_encode($data);
	}
}


 ?>