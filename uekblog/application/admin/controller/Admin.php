<?php 
// 引入命名空间
namespace app\admin\controller;

// 导入系统类
use think\Db;
use think\Controller;

// 声明控制器
class Admin extends Lock{


	// 后台首页方法

	public function index(){

		$search = input("get.search");

		// 从数据库中读取用户

		$list = Db::table("admin")->where("username like '%$search%'")->order("id desc")->paginate(5,false,['query'=>request()->param()]);
		$tot = Db::table("admin")->where("username like '%$search%'")->count();
		// 将数据分配给页面

		$this->assign("list",$list);
		$this->assign("tot",$tot);
		$this->assign("menu","user");

		// 加载页面

		return view();
	}

	// 检测用户名

	public function ajax_username(){
		// 接受提交的数据

		$username  = input("get.username");

		// 查询是否有对应数据

		$data  = Db::table("admin")->where("username = '$username'")->find();

		// 判断是否存在

		if ($data) {
			echo 1;
		}else{
			echo 0;
		}
	}

	// ajax插入数据

	public function ajax_add(){

		// 接受post提交的数据

		$post = input("post.");

		// 判断用户名是否书写

		if ($post['username']) {
			// 判断是否输入密码

			if ($post['password']) {
				// 判断两次密码是否一致

				if ($post['password']  == $post['repassword']) {
					// 插入数据

					$arr['username']=$post['username'];
					$arr['password']=md5($post['password']);
					$arr['status']=$post['status'];
					$arr['time']=time();

					if (Db::table("admin")->insert($arr)) {
						$data = [
							"code"=>200,
							"info"=>"添加成功",
						];					
					}else{

						$data = [
							"code"=>400,
							"info"=>"添加失败",
						];
					}
				}else{
					$data = [
						"code"=>400,
						"info"=>"两次密码不一致",
					];
				}
			}else{
				$data = [
					"code"=>400,
					"info"=>"请输入密码",
				];
			}
		}else{
			$data = [
				"code"=>400,
				"info"=>"请输入用户名",
			];
		}

		echo json_encode($data);

	}

	// 无刷新删除所有

	public function ajax_del_all(){


		// 接受要删除的ID

		$id = input("get.str");

		// 删除所有数据

		if (Db::table("admin")->delete($id)) {
			# code...
			echo 1;
		}else{
			echo 0;
		}

	}

	// 无刷新的获取修改数据

	public function ajax_find(){
		// 获取id
		$id = input("get.id");

		// 从数据库中取数据

		$data = Db::table("admin")->find($id);

		// 数据转换成json

		$this->assign("data",$data);

		return view();
	}

	// 无刷新的修改数据

	public function ajax_save(){
		// 接受数据

		$id = input("post.id");
		$status = input("post.status");
		$password = input("post.password");
		$repassword = input("post.repassword");

		// 判断用户是否修改密码

		if ($password || $repassword) {
			
			// 检测密码长度

			if (strlen($password)>=6 && strlen($password)<=12) {
			 	# code...
			 	// 判断两次密码是否一致

			 	if ($password == $repassword) {
		 		 	$arr=[
		 			 	"id"=>$id,
		 			 	"status"=>$status,
		 			 	"password"=>md5($password),
		 		 	];
			 	}else{
			 		$data = [

			 			"code"=>400,
			 			"info"=>"两次密码不一致"
			 		];

			 		echo json_encode($data);
			 		exit;
			 	}
			 	

			}else{
				$data = [

					"code"=>400,
					"info"=>"密码长度6-12位之间"
				];

				echo json_encode($data);
				exit;
			}
		}else{

			$arr=[
				"id"=>$id,
				"status"=>$status,
			];
		}

		// 在数据库中修改数据
		if (Db::table("admin")->update($arr)) {
			$data = [

				"code"=>200,
				"info"=>"修改成功"
			];
		}else{

			$data = [

				"code"=>400,
				"info"=>"修改失败"
			];
		}

		echo json_encode($data);
		exit;
	}

	// ajax 无刷新修改状态

	public function ajax_status(){

		// 接受get提交的数据

		$data = input("get.");

		// 修改数据

		if (Db::table("admin")->update($data)) {
			return 1;
			# code...
		}else{
			return 0;
		}

	}
}




 ?>