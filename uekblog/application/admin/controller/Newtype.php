<?php 
// 引入命名空间
namespace app\admin\controller;

// 导入系统类
use think\Controller;
use think\Db;

// 声明控制器
class newtype extends Lock{
	// 后台首页方法

	public function index(){

		$list = Db::table("newtype")->order("id DESC")->select();

		// 分配数据

		$this->assign("list",$list);
		$this->assign("menu",'news');
		// 加载页面

		return view();
	}

	// ajax添加的处理

	public function ajax_add(){

		// 接受post请求的数据

		$data = input("post.");

		// 插入是数据库

		if (Db::table("newtype")->insert($data)) {
			# code...
			$arr = [
				"code"=>200,
				"info"=>"添加成功",
			];
		}else{
			$arr = [
				"code"=>400,
				"info"=>"添加失败",
			];
		}

		return $arr;
	}
}




 ?>