<?php 
// 引入命名空间
namespace app\admin\controller;
// 导入数据库类

use think\Db;
// 导入系统类
use think\Controller;

// 声明控制器
class User extends Lock{
	// 后台首页方法

	public function index(){

		$search = input("get.search");
		// 查询所有数据
		// 可以搜索用户名、邮箱和手机号
		$list = Db::table("user")->where("username like '%$search%'")->whereOr("email like '%$search%'")->whereOr("phone like '%$search%'")->order("id DESC")->paginate(5,false,['query'=>request()->param()]);
		
		$tot = Db::table("user")->where("username like '%$search%'")->whereOr("email like '%$search%'")->whereOr("phone like '%$search%'")->count();
		// 分配数据

		$this->assign("list",$list);
		$this->assign("tot",$tot);
		$this->assign("menu","user");
		

		// 加载页面

		return view();
	}
}




 ?>