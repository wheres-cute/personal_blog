<?php 
// 引入命名空间
namespace app\admin\controller;

// 导入系统类
use think\Controller;

// 导入数据库类
use think\Db;

// 声明控制器
class Comment extends Lock{
	// 后台首页方法

	public function index(){

		// 查询数据

		$data = Db::table("comment")
			->field("comment.*,news.title,news.img,user.username")
			->join("news",'comment.nid = news.id')
			->join("user",'comment.uid = user.id')
			->order("comment.id DESC")
			->paginate(3,false,['query'=>request()->param()]);


		$tot = Db::table("comment")
			->field("comment.*,news.title,news.img,user.username")
			->join("news",'comment.nid = news.id')
			->join("user",'comment.uid = user.id')
			->order("comment.id DESC")
			->count();


		// 分配数据

		$this->assign("data",$data);
		$this->assign("tot",$tot);
		$this->assign("menu",'news');

		// 加载页面

		return view();
	}

	// 无刷新修改状态

	public function ajax_status(){
		// 接受参数

		$data = input("get.");

		// 发送sql语句修改

		if (Db::table("comment")->update($data)) {
			$arr =[
				"code"=>200,
				"info"=>"修改成功",
			];
		}else{
			$arr =[
				"code"=>400,
				"info"=>"修改失败",
			];
		}

		return $arr;
	}
}




 ?>