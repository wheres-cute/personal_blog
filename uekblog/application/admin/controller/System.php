<?php 
// 引入命名空间
namespace app\admin\controller;

// 导入系统类
use think\Controller;

// 声明控制器
class System extends Lock{
	// 后台首页方法
	// 读取扩展配置文件中的内容
	// 需要在E:\wamp64\www\uekblog\application\extra文件夹下新建 webConfig.php
	// 在webConfig中书写网站的配置信息 

	public function index(){
		$this->assign("menu","sys");

		// 加载页面
		return view();
	}


	// 处理配置信息修改

	public function check(){


		// 获取post提交的数据

		$data = input("post.");

		// 修改webConfig中内容

		$config = config("webConfig");

		// 数组合并

		$arr=array_merge($config,$data);

		// 处理文件上传
		$file = request()->file("LOGO");

		if ($file) {
			// 开始上传文件的操作

			$info = $file->move(ROOT_PATH.'public/upload/');
			
			// 判断是否上传成功

			if ($info) {
				# code...

				$arr['LOGO']=$info->getSaveName();
			}else{

				$this->error($file->getError());
			}
		}


		// 将数组转换成字符串输出到文件中

		$str = "<?php return ".var_export($arr,true).";";

		// 将字符串内容写入到webConfig配置文件中
		if(file_put_contents("../application/extra/webConfig.php", $str)){

			// 删除图片
			if($file){
				unlink("./upload/".$config['LOGO']);
			}
			$this->redirect("system/index");
		}else{
			$this->redirect("system/index");

		}

	}
}




 ?>