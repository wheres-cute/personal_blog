<?php 
// 命名空间

namespace app\admin\controller;

// 导入系统控制器

use think\Controller;
use think\Db;

// 声明控制器

class Login extends Controller{

	// 登录页面

	public function index(){
		return view();
	}

	// 处理登录

	public function check(){

		// 接收post提交的数据

		$username = input("post.username");
		$password = input("post.password");
		$code = input("post.code");

		// 判断是否输入用户名

		if ($username) {
			// 判断师傅输入密码
			if ($password) {
				// 判断是否输入验证码
				if ($code) {
					// 判断验证码是否正确

					if(captcha_check($code)){

						// 从数据库中检测

						$where = [
							"username"=>$username,
							"password"=>md5($password),
							"status"=>0,
						];
						$data = Db::table("admin")->where($where)->find();
					
						// 判断是否登录

						if ($data) {
							// 把信息存到session中
							session("uekblog_message_username",$data['username']);
							session("uekblog_message_id",$data['id']);

							// 更新最后一次登录时间
							$arr = [
								"id"=>$data['id'],
								"logintime"=>time()
							];

							Db::table("admin")->update($arr);
							$this->success("登录成功","index/index");
						}else{
							$this->error("登录失败");

						}

					}else{
						$this->error("验证码错误");

					}
				}else{
					$this->error("请输入验证码");

				}
			}else{
				$this->error("请输入密码");
			}
		}else{
			$this->error("请输入用户名");
		}


	}

	// 用户退出

	public function logout(){
		session(null);
		$this->success("退出成功","Login/index");

	}
}


 ?>