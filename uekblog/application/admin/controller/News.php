<?php 
// 引入命名空间
namespace app\admin\controller;

// 导入系统类
use think\Controller;

use think\Db;

// 声明控制器
class News extends Lock{
	// 后台首页方法

	public function index(){

		// 接收typeid

		$typeid = input("get.typeid");
		$search = input("get.search");
		$sort = input("get.sort");



		// 声明空数组

		$where=[];

		// 如果typeid
		if ($typeid) {
			# code...
			$where['news.typeid'] = $typeid;
		}

		// 排序

		switch($sort){
			case 0:
				$str = "news.id DESC";
			break;
			case 1:
			# code...
				$str = "news.num DESC";

			break;
			case 2:
				# code...
				$str = "news.zannum DESC";

			break;
			case 3:
			# code...
				$str = "news.shounum DESC";

			break;

			default:
				$str = "news.id DESC";

			break;
		}




		// 查询数据

		$type = Db::table("newtype")->select();
		$list = Db::table("news")
					->where($where)
					->where("news.title like '%$search%'")
					->field("news.*,newtype.name")
					->join("newtype","newtype.id = news.typeid")
					->order($str)
					->paginate(3,false,['query'=>request()->param()]);


		$tot = Db::table("news")
				->where($where)
				->where("news.title like '%$search%'")
				->join("newtype","newtype.id = news.typeid")
				->count();



		$this->assign("tot",$tot);
		$this->assign("list",$list);
		$this->assign("type",$type);
		$this->assign("typeid",$typeid);
		$this->assign("sort",$sort);
		// 分配当前菜单

		$this->assign("menu",'news');

		// 加载页面

		return view();
	}

	// 添加新闻的页面

	public function add(){

		// 查询所有新闻分类

		$newtype = Db::table("newtype")->select();

		$this->assign("data",$newtype);
		// 分配当前菜单

		$this->assign("menu",'news');

		// 加载页面
		return view();
	}

	// 文件上传

	public function upload(){

		// 获取用户上传的内容

		$file = request()->file("Filedata");

		// 判断是否上传图片

		if ($file) {
			// 移动文件
			$info = $file->move(ROOT_PATH.'/public/upload/tmp');

			// 如果上传成功

			if ($info) {
				return $info->getSaveName();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	// 文件上传方法

	public function insert(){
		// 获取post提交的所有数据

		$data = input("post.");

		$data['time']=time();

		// 直接插入数据

		if (Db::table("news")->insert($data)) {

			// 将图片从临时目录转移news目录

			$tmp ="./upload/tmp/".$data['img'];
			$new = "./upload/news/".$data['img'];

			// 获取新的文件部分

			$dir = dirname($new);

			// 判断文件是否存在
			if (!file_exists($dir)) {
				mkdir($dir);
			}
			// 判断目录是否存在	

			copy($tmp,$new);
			$this->redirect("news/index");
		}else{
			$this->redirect("news/add");

		}
	}
}




 ?>